package com.safemessenger.api;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.JsonObject;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;


@Path("/users")
public class UsersResource {

	DB		db = DBDriver.getInstance().getDb();
	
	public static BasicDBObject		getUserByToken(String token) {
		BasicDBObject	ret = null;
		String			name = extractFromToken("name", token), password = extractFromToken("password", token);
		DBCursor 		cursor;
		BasicDBObject	query = new BasicDBObject()
				.append("name", name)
				.append("password", PasswordManager.encryptUserPassword(name, password));
		
		cursor = DBDriver.getInstance().getDb().getCollection("users").find(query);
		if (cursor.hasNext())
			ret = (BasicDBObject) cursor.next();
		return ret;
	}
	
	public static BasicDBObject		getUserByName(String name) {
		BasicDBObject	ret = null;
		DBCursor 		cursor;
		BasicDBObject	query = new BasicDBObject()
				.append("name", name);
		
		cursor = DBDriver.getInstance().getDb().getCollection("users").find(query);
		if (cursor.hasNext())
			ret = (BasicDBObject) cursor.next();
		return ret;
	}
	
	@Path("/signup")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public	String	signUp(@HeaderParam("Authorization") String authorization) {
		String			name = extractFromToken("name", authorization), password = extractFromToken("password", authorization);
		JsonObject		ret = new JsonObject();
		DBCollection	users;
		BasicDBObject	query, newUser;
		DBCursor		cursor;

		ret.addProperty("request", "sign up");
		if ((name.length() > 15 || name.length() < 4) || (password.length() > 15 || password.length() < 4)) {
			ret.addProperty("signed up", "KO");
			ret.addProperty("error", "Incorrect length for name or password");
		}
		else {
			users = db.getCollection("users");
			query = new BasicDBObject().append("name", name);
			cursor = users.find(query);
			if (cursor.hasNext()) {
				ret.addProperty("status", "KO");
				ret.addProperty("error", "Name already taken");
			}
			else {
				newUser = new BasicDBObject();
				newUser.append("name", name)
				.append("password", PasswordManager.encryptUserPassword(name, password))
				.append("friends", new BasicDBList());
				users.insert(newUser);
				ret.addProperty("status", "OK");
				ret.addProperty("token", authorization);
			}
		}
		return ret.toString();
	}

	
	
	public	static	String	extractFromToken(String element, String token) {
		String		ret = new String(Base64.decode(token.substring(6)));
		
		if (element == "name")
			ret = ret.substring(0, ret.indexOf(":"));
		else if (element == "password")
			ret = ret.substring(ret.indexOf(":"), ret.length());
		return ret;
	}
	
	@Path("/signin")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String		signIn(@HeaderParam("Authorization") String authorization) {
		String			name = extractFromToken("name", authorization);
		String 			password= extractFromToken("password", authorization);
		JsonObject		ret = new JsonObject();
		DBCollection	users = db.getCollection("users");
		BasicDBObject	query;
		DBCursor		cursor;
		
		query = new BasicDBObject()
		.append("name", name)
		.append("password", PasswordManager.encryptUserPassword(name, password));
		cursor = users.find(query);
		
		ret.addProperty("request", "sign in");
		if (cursor.hasNext()) {
			ret.addProperty("status", "OK");
			ret.addProperty("token", authorization);
		}
		else {
			ret.addProperty("status", "KO");
			ret.addProperty("error", "Incorrect username or password");
		}
		return ret.toString();
	}

}
