package com.safemessenger.api;

import java.net.UnknownHostException;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;

public class DBDriver {

	private static DBDriver	instance = null;
	private MongoClient		mongoClient;
	
	public static DBDriver	getInstance() {
		return instance == null ? new DBDriver() : instance;
	}
	
	private 		DBDriver() {
		MongoClientOptions mco = new MongoClientOptions.Builder()
	    .connectionsPerHost(100)
	    .threadsAllowedToBlockForConnectionMultiplier(10)
	    .build();
		
		try {
			mongoClient = new MongoClient(new ServerAddress("localhost", 27017), mco);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
	public DB 		getDb() {
		return mongoClient.getDB("safemessenger");
	}
	
}
