package com.safemessenger.api;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;

@Path("/friends")
public class FriendsResource {

	DB			db = DBDriver.getInstance().getDb();
	
	@Path("/add")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public 	String		addFriend(@HeaderParam("Authorization") String authorization, @FormParam("friendname") String friendName) {
		JsonObject		json = new JsonObject();
		BasicDBObject	currentUser = UsersResource.getUserByToken(authorization);
		BasicDBObject	userToAdd = UsersResource.getUserByName(friendName);
		
		json.addProperty("request", "add friend");
		if (userToAdd == null) {
			json.addProperty("status", "KO");
			json.addProperty("error", "No such user " + friendName);
			return json.toString();
		}
		else if (friendName.equals(currentUser.getString("name"))) {
			json.addProperty("status", "KO");
			json.addProperty("error", "You cannot add yourself as a friend");
			return json.toString();
		}
		else if (((BasicDBList) currentUser.get("friends")).contains(friendName)) {
			json.addProperty("status", "KO");
			json.addProperty("error", "You already have this user as a friend");
			return json.toString();
		}
		json.addProperty("status", "OK");
		DBDriver.getInstance().getDb().getCollection("users").findAndRemove(currentUser);
		((BasicDBList) currentUser.get("friends")).add(friendName);
		DBDriver.getInstance().getDb().getCollection("users").insert(currentUser);
		return json.toString();
	}
	
	@Path("remove")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String	removeFriend(@HeaderParam("Authorization") String authorization, @FormParam("friendname") String friendName) {
		JsonObject		json = new JsonObject();
		BasicDBObject	currentUser = UsersResource.getUserByToken(authorization);
		
		json.addProperty("request", "remove friend");
		if (!((BasicDBList)currentUser.get("friends")).contains(friendName)) {
			json.addProperty("status", "KO");
			json.addProperty("error", "You do not have " + friendName + " in your friend list");
			return json.toString();
		}
		json.addProperty("status", "OK");
		DBDriver.getInstance().getDb().getCollection("users").findAndRemove(currentUser);
		((BasicDBList) currentUser.get("friends")).remove(friendName);
		DBDriver.getInstance().getDb().getCollection("users").insert(currentUser);
		return json.toString();
	}
	
	@Path("/get")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String		getFriends(@HeaderParam("Authorization") String authorization) {
		JsonObject		json = new JsonObject();
		BasicDBObject	user = UsersResource.getUserByToken(authorization);
		BasicDBList		friendsList = (BasicDBList) user.get("friends");
		JsonParser		jsonParser = new JsonParser();
		JsonElement		tradeElement = jsonParser.parse(friendsList.toString());
		JsonArray		array = tradeElement.getAsJsonArray();
		
		json.addProperty("request", "get friends");
		json.addProperty("status", "OK");
		json.add("friends", array);
		return json.toString();
	}
	
}
