package com.safemessenger.api;

import java.util.Calendar;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.bson.types.ObjectId;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;

@Path("/messages")
public class MessagesResource {

	@Path("/remove")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String 		removeMessage(@HeaderParam("Authorization") String authorization, @FormParam("_id") String id) {
		JsonObject		json = new JsonObject();
		BasicDBObject	query;
		DBCursor		cursor;
		Integer			i = 0;
		
		json.addProperty("request", "remove message");
		query = new BasicDBObject()
			.append("_id", new ObjectId(id));
		cursor = DBDriver.getInstance().getDb().getCollection("messages").find(query);
		if (cursor.hasNext()) {
			json.addProperty("status", "OK");		
			DBDriver.getInstance().getDb().getCollection("messages").remove(cursor.next());
		}
		else {
			json.addProperty("status", "KO");
			json.addProperty("error", "Message not found");
		}
		return json.toString();
	}
	
	@Path("/send")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String		sendMessage(@HeaderParam("Authorization") String authorization, @FormParam("receiver") String receiver, @FormParam("content") String content) {
		JsonObject		json = new JsonObject();
		BasicDBObject	currentUser = UsersResource.getUserByToken(authorization);
		BasicDBObject	newMessage;
		
		json.addProperty("request", "send message");
		if (content.length() == 0) {
			json.addProperty("status", "KO");
			json.addProperty("error", "Content length must be at least of 1 character");
			return json.toString();
		}
		json.addProperty("status", "OK");
		newMessage = new BasicDBObject()
			.append("sender", currentUser.getString("name"))
			.append("receiver", receiver)
			.append("content", content)
			.append("timestamp", Calendar.getInstance().getTimeInMillis());
		DBDriver.getInstance().getDb().getCollection("messages").insert(newMessage);
		return json.toString();
	}
	
	@Path("/get")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public	String		getMessages(@HeaderParam("Authorization") String authorization, @QueryParam("from") String from) {
		JsonObject		json = new JsonObject();
		String			currentUserName = UsersResource.getUserByToken(authorization).getString("name");
		JsonArray		messagesArray = new JsonArray();
		DBCursor		cursor;
		BasicDBObject	query;
		BasicDBObject	currentDBMessage;
		JsonObject		currentMsg;
		
		json.addProperty("request", "get messages");
		if (!from.equals("all")) {
			query = new BasicDBObject()
			.append("sender", from)
			.append("receiver", currentUserName);
			cursor = DBDriver.getInstance().getDb().getCollection("messages").find(query);
			while (cursor.hasNext()) {
				currentDBMessage = (BasicDBObject) cursor.next();
				currentMsg = new JsonObject();
				currentMsg.addProperty("sender", from);
				currentMsg.addProperty("timestamp", currentDBMessage.getLong("timestamp"));
				currentMsg.addProperty("receiver", currentUserName);
				currentMsg.addProperty("_id", currentDBMessage.getString("_id"));
				currentMsg.addProperty("content", currentDBMessage.getString("content"));
				messagesArray.add(currentMsg);
			}
			query = new BasicDBObject()
				.append("sender", currentUserName)
				.append("receiver", from);
			cursor = DBDriver.getInstance().getDb().getCollection("messages").find(query);
			while (cursor.hasNext()) {
				currentDBMessage = (BasicDBObject) cursor.next();
				currentMsg = new JsonObject();
				currentMsg.addProperty("sender", currentUserName);
				currentMsg.addProperty("timestamp", currentDBMessage.getString("timestamp"));
				currentMsg.addProperty("receiver", from);
				currentMsg.addProperty("_id", currentDBMessage.getString("_id"));
				currentMsg.addProperty("content", currentDBMessage.getString("content"));
				messagesArray.add(currentMsg);
			}
		}
		else {
			query = new BasicDBObject()
			.append("receiver", currentUserName);
			cursor = DBDriver.getInstance().getDb().getCollection("messages").find(query);
			// get les messages re�us
			while (cursor.hasNext()) {
				currentMsg = new JsonObject();
				currentDBMessage = (BasicDBObject) cursor.next();
				currentMsg.addProperty("sender", currentDBMessage.getString("sender"));
				currentMsg.addProperty("receiver", currentUserName);
				currentMsg.addProperty("_id", currentDBMessage.getString("_id"));
				currentMsg.addProperty("timestamp", currentDBMessage.getLong("timestamp"));
				currentMsg.addProperty("content", currentDBMessage.getString("content"));
				messagesArray.add(currentMsg);
			}
			// get les messages envoy�s
			query = new BasicDBObject()
				.append("sender", currentUserName);
			cursor = DBDriver.getInstance().getDb().getCollection("messages").find(query);
			while (cursor.hasNext()) {
				currentMsg = new JsonObject();
				currentDBMessage = (BasicDBObject) cursor.next();
				currentMsg.addProperty("sender", currentDBMessage.getString("sender"));
				currentMsg.addProperty("receiver", currentDBMessage.getString("receiver"));
				currentMsg.addProperty("content", currentDBMessage.getString("content"));
				currentMsg.addProperty("_id", currentDBMessage.getString("_id"));
				currentMsg.addProperty("timestamp", currentDBMessage.getLong("timestamp"));
				messagesArray.add(currentMsg);
			}
		}
		json.addProperty("status", "OK");
		json.add("messages", messagesArray);
		return json.toString();
	}
}
