package com.safemessenger.api;


public class PasswordManager {

	public static final	String	API_SALT = "safemsgrftw";
	
	public static String		encryptUserPassword(String name, String password) {
		return org.apache.commons.codec.digest.DigestUtils.sha256Hex(API_SALT + name + password);
	}
	
}
