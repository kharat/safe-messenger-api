import java.util.ArrayList;
import java.util.List;


public class User {

	String			name;
	List<String>	friends;
	
	User() {
		name = "";
		friends = new ArrayList<String>();
	}
	
	User(String name, List<String> friends) {
		this.name = name;
		this.friends = friends;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getFriends() {
		return friends;
	}

	public void setFriends(List<String> friends) {
		this.friends = friends;
	}
	
}
